# micro-script

## Overview

A work-in-progress demo compiler for a simple imperative language, using parser combinators in Haskell. Things are generally untidy and hard to read currently.

Work on the parser stage is nearly complete.

## Example

This Euclidean algorithm implementation:

    func example: return gcd(6,2)

    func gcd a b:
        while (b /= 0):
            var t = b
            b -> a %  # Section syntax: same as 'b = a % b'
            a = t
        return a

is lexed into this stream of tokens:

    [TMargin 0,TLone KeyFunc,TIdent "example",TLone ColonSep,TLone KeyReturn,TIdent "gcd",TLone ParenL,TNum 6,TLone CommaSep,TNum 2,TLone ParenR,TLone EndLine
    ,TMargin 0,TLone KeyFunc,TIdent "gcd",TIdent "a",TIdent "b",TLone ColonSep,TLone EndLine
    ,TMargin 4,TLone KeyWhile,TLone ParenL,TIdent "b",TBinOp BinopNeq,TNum 0,TLone ParenR,TLone ColonSep,TLone EndLine
    ,TMargin 8,TLone KeyVar,TIdent "t",TAssign AsopSet,TIdent "b",TLone EndLine
    ,TMargin 8,TIdent "b",TAssign AsopModify,TIdent "a",TBinOp BinopMod,TLone EndLine
    ,TMargin 8,TIdent "a",TAssign AsopSet,TIdent "t",TLone EndLine
    ,TMargin 4,TLone KeyReturn,TIdent "a",TLone EndLine]
    
which is parsed into this syntax tree:
    
    Program 
        [FuncDef "example" [] (SBlock
            [SInline (SinReturn (ExFuncApp (FuncApp "gcd" [ExLit (LitInt 6),ExLit (LitInt 2)])))]
        ,FuncDef "gcd" ["a","b"] (SBlock
            [SWhile (ExBinApp BinopNeq (ExIdent "b") (ExLit (LitInt 0))) (SBlock
                [SInline (SinDecl "t" (ExIdent "b"))
                ,SInline (SinModify "b" (UfValOp (ExIdent "a") BinopMod))
                ,SInline (SinAssign "a" (ExIdent "t"))])
            ,SInline (SinReturn (ExIdent "a"))])]