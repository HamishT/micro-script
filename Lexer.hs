module Lexer
    ( lexer
    ) where

import Combinator
import Grammar
import MathUtil

import Data.Foldable (asum)
import Data.Functor (($>))
import Data.Monoid (Alt(..))

import Text.Read (readMaybe)

-- import Debug.Trace

lexer :: Parser Char [Token]
lexer = concat <$> many lexLine

-- TODO: make this less confusing
-- key: differentiate null-line and line-with-tokens cases
lexLine :: Parser Char [Token]
lexLine = reject endStream >> (nil <|> tokens)
    where
        nil = [] <$ endLine
        endLine = token $ (match '#' >> () <$ untilMany endTok) <|> endTok
        endTok = endStream <|> (() <$ eol)
        tokens = do
            n <- count (many space)
            ts <- some (token lexToken)
            endLine
            return $ (TMargin n : ts) ++ [TLone EndLine]

lexToken :: Parser Char Token
lexToken = single <|> numLit <|> operator <|> idents

single :: Parser Char Token
single = TLone <$> (ones <|> strs)
    where
        ones = associate match ok ov
        strs = associate string sk sv
        ok = [','     , ':'     , '('   , ')'   , '{'  , '}'  , '['   , ']'   ]
        ov = [CommaSep, ColonSep, ParenL, ParenR, MaskL, MaskR, ArrayL, ArrayR]
        sk = ["if" , "else" , "while" , "for" , "true" , "false" , "func" , "var" , "return" ]
        sv = [KeyIf, KeyElse, KeyWhile, KeyFor, KeyTrue, KeyFalse, KeyFunc, KeyVar, KeyReturn]

numLit :: Parser Char Token
numLit = do
    pre <- optional (match '0' >> anyOf "bx")
    let (reader, parser) = case pre of
         Nothing  -> (readDec, digit)
         Just 'b' -> (readBin, anyOf "01")
         Just 'x' -> (readHex, digit <|> anyOf (['a'..'f']++['A'..'F']))
    TNum <$> ifJust reader (some parser)

operator :: Parser Char Token
operator = TAssign <$> (AsopModify <$ string "->")
       <|> TBinOp <$> associate string sk sv
       <|> TAssign <$> (AsopSet <$ match '=')
       <|> TUnOp <$> (UnopInvert <$ match '!')
       <|> TBinOp <$> associate match ok ov
    where
        sk = ["==","/=",">=","<=","<<",">>","->>","<<<",">>>"]
        sv = [BinopEq,BinopNeq,BinopGtE,BinopLtE,BinopShL,BinopShR,BinopShRA,BinopRotL,BinopRotR]
        ok = "+-*/<>%"
        ov = [BinopPlus,BinopMinus,BinopMult,BinopDiv,BinopLt,BinopGt,BinopMod]

idents :: Parser Char Token
idents = TIdent <$> ((:) <$> firstChar <*> many alphaDigit)
    where
        firstChar = match '_' <|> alpha