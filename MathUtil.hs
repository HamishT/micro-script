module MathUtil
    ( readBin
    , readDec
    , readHex
    , associate
    ) where

import Combinator (Parser)

import Control.Applicative (liftA2, Alternative, (<|>))

import Data.Char (toUpper)
import Data.Foldable (asum)
import Data.Functor (($>))
import Data.Semigroup ((<>))

import Text.Read (readMaybe)

readBin :: String -> Maybe Integer
readBin = readBase 2 (['0','1'] ~> [0,1])

readDec :: String -> Maybe Integer
readDec = readBase 10 (['0'..'9'] ~> [0..9])

readHex :: String -> Maybe Integer
readHex = readBase 16 (['0'..'9'] ~> [0..9] <||> ['A'..'F'] ~> [10..15]) . fmap toUpper
    where (<||>) = liftA2 (<|>)
          infixl 2 <||>

associate :: (a -> Parser s c) -> [a] -> [b] -> Parser s b
associate pf ks vs = asum $ zipWith ($>) (pf <$> ks) vs

readBase :: Integer -> (Char -> Maybe Integer) -> String -> Maybe Integer
readBase base convert = fmap (foldl (\x y -> base*x + y) 0) . traverse convert

(~>) :: [Char] -> [Integer] -> Char -> Maybe Integer
(k ~> v) x = lookup x (zip k v)
infixl 3 ~>