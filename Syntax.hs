{-# LANGUAGE TupleSections #-}

module Syntax (syntax) where

import Combinator
import Grammar
import Lexer
import MathUtil

import Control.Monad (guard)

import Data.List (drop, reverse)

import Debug.Trace

--syntax :: Parser Token Program

-- Current best solution to simplify matching: simple lens implementation

test' p f str = do
    let ts = f <$> runParser lexer str
    putStrLn "Tokens:" >> print ts
    let prog = runParser p =<< ts
    putStrLn "Program:" >> print prog

test p = test' p id
testline p = test' p (drop 1 . reverse . drop 1 . reverse)
testend p = test' p (drop 1) 

data TokenLens a = TokenLens { tget :: Parser Token a, (!) :: a -> Parser Token a }
infixl 9 !
mkLens m c = TokenLens m (\x -> x <$ match (c x))
tlone = mkLens (do { TLone x <- feed; return x }) TLone
tnum = mkLens (do { TNum x <- feed; return x }) TNum
tident = mkLens (do { TIdent x <- feed; return x}) TIdent
tassign = mkLens (do { TAssign x <- feed; return x}) TAssign
tunop = mkLens (do { TUnOp x <- feed; return x}) TUnOp
tbinop = mkLens (do { TBinOp x <- feed; return x}) TBinOp
tmargin = mkLens (do { TMargin x <- feed; return x}) TMargin

syntax :: Parser Token Program
syntax = Program <$> many funcDef

funcDef :: Parser Token FuncDef
funcDef = do
    many (tlone!EndLine) >> tmargin!0 >> tlone!KeyFunc
    FuncDef <$> tget tident <*> many (tget tident) <*> sBlock 0

sBlock :: Integer -> Parser Token SBlock
sBlock margin = SBlock <$> (tlone!ColonSep >> (one <|> more))
    where
        one = pure . SInline <$> (inStmt <* tlone!EndLine)
        more = do
            tlone!EndLine
            margin' <- peek (tget tmargin)
            guard (margin' > margin)
            many (tmargin!margin' >> stmt margin')


mask :: Parser Token Mask
mask = maskOne <|> maskAll 
    where
        maskOne = do
            op <- wrap (tlone!MaskL) (tlone!ColonSep) bitOp
            ns <- separateSome (tget tnum) (tlone!CommaSep)
            tlone!MaskR
            return $ Mask ((op,) <$> ns)
        maskAll = do
            tlone!MaskL
            res <- separateSome (flip (,) <$> tget tnum <*> bitOp) (tlone!CommaSep)
            tlone!MaskR
            return $ Mask res
        bitOp = associate (match . TIdent) ["s","c","t"] [BopSet,BopClear,BopFlip]

funcApp :: Parser Token FuncApp
funcApp = FuncApp <$> tget tident <*> parens (separateMany expr (tlone!CommaSep))

unFunc :: Parser Token UnFunc
unFunc = UfOpVal <$> tget tbinop <*> expr
     <|> UfValOp <$> expr <*> tget tbinop
     <|> UfCall <$> tget tident
     <|> UfMask <$> mask

litVal :: Parser Token LitVal
litVal = LitInt <$> tget tnum
     <|> LitBool False <$ tlone!KeyFalse
     <|> LitBool True  <$ tlone!KeyTrue

-- needs expr and expr' to prevent recursion from `expr = chainlSome expr ...`
expr :: Parser Token Expr
expr = chainlSome expr' (ExBinApp <$> tget tbinop) <|> parens expr'
    where
        expr' = ExUnApp <$> tget tunop <*> expr
            <|> ExMaskApp <$> mask <*> expr
            <|> ExFuncApp <$> funcApp
            <|> ExIdent <$> tget tident
            <|> ExLit <$> litVal

inStmt :: Parser Token InStmt
inStmt = SinAssign <$> tget tident <*> (tassign!AsopSet *> expr)
     <|> SinModify <$> tget tident <*> (tassign!AsopModify *> unFunc)
     <|> SinDecl <$> (tlone!KeyVar *> tget tident) <*> (tassign!AsopSet *> expr)
     <|> SinReturn <$> (tlone!KeyReturn *> expr)
     <|> SinCall <$> funcApp
     -- <|> return SinNil

stmt :: Integer -> Parser Token Stmt
stmt margin = SInline <$> (inStmt <* (tlone!EndLine))
          <|> tlone!KeyWhile *> (SWhile <$> parens expr <*> sBlock margin)
          <|> tlone!KeyFor *> sfor
          <|> tlone!KeyIf *> (sife <|> sif)
    where
        sfor = do
            s0 <- tlone!ParenL *> inStmt
            ex <- tlone!CommaSep *> expr
            s1 <- tlone!CommaSep *> inStmt
            sb <- tlone!ParenR *> sBlock margin
            return $ SFor s0 ex s1 sb
        sife = do
            e <- parens expr
            sbTrue <- sBlock margin
            tlone!EndLine >> tmargin!margin >> tlone!KeyElse
            sbFalse <- sBlock margin
            return $ SIfElse e sbTrue sbFalse
        sif = SIf <$> parens expr <*> sBlock margin

parens :: Parser Token a -> Parser Token a
parens = wrap (tlone!ParenL) (tlone!ParenR)