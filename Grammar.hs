module Grammar where

-- Grammar issues at present:
-- 1) Lexer differentiation between binary and unary ops prevents usage of (-) as both:
--    Unary usage disabled for now
-- 2) Mask bitops use TIdent tokens since they're alpha
--    Seems a bit hacky

-- Common

type Ident = String
--type Type = String

-- should have '| UnopNegate' in future
data UnaryOperator = UnopInvert
                   deriving (Show, Eq)

data BinaryOperator = BinopPlus | BinopMinus | BinopMult | BinopDiv
                    | BinopEq | BinopNeq | BinopLt | BinopLtE | BinopGt | BinopGtE
                    | BinopShL | BinopShR | BinopShRA | BinopRotL | BinopRotR
                    | BinopMod
                    deriving (Show, Eq)

data AssignOperator = AsopSet | AsopModify
                    deriving (Show, Eq)

-- Lexer data

data Token = TLone LoneToken
           | TNum Integer
           | TIdent String
           | TAssign AssignOperator
           | TUnOp UnaryOperator
           | TBinOp BinaryOperator
           | TMargin Integer
           deriving (Show, Eq)

data LoneToken = CommaSep | ColonSep | EndLine
               | ParenL   | ParenR
               | MaskL    | MaskR
               | ArrayL   | ArrayR
               | KeyIf    | KeyElse | KeyWhile | KeyFor
               | KeyTrue  | KeyFalse
               | KeyFunc  | KeyVar  | KeyReturn
               deriving (Show, Eq)


-- Syntax data

type Index = Integer

newtype Program = Program [FuncDef]
    deriving (Show, Eq)

data FuncDef = FuncDef Ident [Ident] SBlock
    deriving (Show, Eq)

newtype Mask = Mask [(BitOp,Index)]
    deriving (Show, Eq)

data FuncApp = FuncApp Ident [Expr]
    deriving (Show, Eq)

data BitOp = BopSet | BopClear | BopFlip
    deriving (Show, Eq)

newtype SBlock = SBlock [Stmt]
    deriving (Show, Eq)

data InStmt = SinAssign Ident Expr
            | SinModify Ident UnFunc
            | SinDecl Ident Expr
            | SinReturn Expr
            | SinCall FuncApp
            | SinNil
    deriving (Show, Eq)

data Stmt = SInline InStmt
          | SWhile Expr SBlock
          | SFor InStmt Expr InStmt SBlock
          | SIf Expr SBlock
          | SIfElse Expr SBlock SBlock
          deriving (Show, Eq)

data UnFunc = UfOpVal BinaryOperator Expr
            | UfValOp Expr BinaryOperator
            | UfCall Ident
            | UfMask Mask
            deriving (Show, Eq)

data LitVal = LitInt Integer
            | LitBool Bool
            deriving (Show, Eq)

-- suggestion: ternary operator?

data Expr = ExLit LitVal
          | ExIdent Ident
          | ExUnApp UnaryOperator Expr
          | ExMaskApp Mask Expr
          | ExBinApp BinaryOperator Expr Expr
          | ExFuncApp FuncApp
          deriving (Show, Eq)