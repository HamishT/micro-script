{-# LANGUAGE BangPatterns #-}

module Combinator (
    Parser,
    runParser,
    feed,
    peek,
    endStream,
    eol,
    satisfy,
    reject,
    untilSome, untilMany,
    ifJust,
    count,
    transform,
    match,
    some, many, optional, (<|>), empty,
    exactly,
    atLeast,
    atMost,
    atLeastMost,
    alpha,
    digit,
    alphaDigit,
    natural,
    signed,
    space,
    anyOf,
    string,
    strings,
    token,
    separateSome,
    separateMany,
    chainlSome, chainlMany, chainrSome, chainrMany,
    wrap, wrapMatch, wrapNested
    ) where

-- optional is re-exported
import Control.Applicative (Alternative(..), liftA2, optional)
import Control.Monad (MonadPlus(..), guard, replicateM, (>=>))

import Data.Bifunctor (first)
import Data.Char (isAlpha, isDigit, isSpace)
import Data.Foldable (asum)
import Data.List (uncons, stripPrefix, takeWhile, dropWhileEnd)
import Data.Maybe (maybe, fromMaybe, isJust)

import Debug.Trace (traceShow)

import Text.Read (readMaybe)

newtype Parser s a = P { unP :: [s] -> Maybe (a,[s]) }

runParser :: Parser s a -> [s] -> Maybe a
runParser (P f) s = case f s of
    Just (x,[]) -> Just x
    _           -> Nothing

feed :: Parser s s
feed = P uncons

peek :: Parser s a -> Parser s a
peek (P f) = P $ \s0 -> do
    (x, s1) <- f s0
    return (x, s0)

endStream :: Parser s ()
endStream = optional feed >>= maybe (return ()) (const empty)

transform :: (s -> Maybe a) -> Parser s a
transform f = maybe empty pure . f =<< feed
-- Alternate internal def using maybe monad:
-- transform f = P $ \s -> do
--     (c,cs) <- uncons s
--     x <- f c
--     return (x,cs)

ifJust :: (a -> Maybe b) -> Parser s a -> Parser s b
ifJust f p = maybe empty pure . f =<< p

satisfy :: (s -> Bool) -> Parser s s
satisfy f = do
    c <- feed
    guard (f c)
    return c

noSatisfy :: (s -> Bool) -> Parser s s
noSatisfy = satisfy . fmap not

match :: Eq s => s -> Parser s s
match = satisfy . (==)

avoid :: Eq s => s -> Parser s s
avoid = noSatisfy . (==) -- satisfy . (/=)

-- possibly confusing semantics:
-- empty if p matches; succeeds but does not advance stream if p fails
reject :: Parser s a -> Parser s ()
reject p = optional p >>= maybe (pure ()) (const empty)

untilSome :: Parser s a -> Parser s ([s],a)
untilSome p = (,) <$> some (reject p *> feed) <*> p

untilMany :: Parser s a -> Parser s ([s],a)
untilMany p = (,) <$> many (reject p *> feed) <*> p

count :: Parser s [a] -> Parser s Integer
count = fmap (toInteger . length)

-- optional :: Parser s a -> Parser s (Maybe a)
-- optional p = fmap Just p <|> pure Nothing
-- Alternate internal def:
-- optional (P f) = P $ \s -> Just $ case f s of
--     Nothing      -> (Nothing, s)
--     Just (x, s') -> (Just x, s')

occurs :: Parser s a -> Parser s Bool
occurs = fmap isJust . optional

orMempty :: Monoid a => Parser s a -> Parser s a
orMempty = withDefault mempty

withDefault :: a -> Parser s a -> Parser s a
withDefault v p = p <|> pure v

separateMany :: Parser s a -> Parser s b -> Parser s [a]
separateMany px ps = orMempty (separateSome px ps)

separateSome :: Parser s a -> Parser s b -> Parser s [a]
separateSome px ps = (:) <$> px <*> orMempty (ps >> separateSome px ps)

alpha :: Parser Char Char
alpha = satisfy (isAlpha)

digit :: Parser Char Char
digit = satisfy (isDigit)

alphaDigit :: Parser Char Char
alphaDigit = alpha <|> digit

space :: Parser Char Char
space = match ' '

eol :: Parser Char String
eol = string "\r\n" <|> ((:[]) <$> match '\n')

token :: Parser Char a -> Parser Char a
token p = many space >> p

internalExpect :: (a -> Maybe b) -> String -> Parser s a -> Parser s b
internalExpect f err = fmap (fromMaybe (error err) . f)

signed :: Parser Char Integer
signed = let msg = "fatal: signed parse failed"
             p = ((:) <$> withDefault ' ' (match '-') <*> some digit)
         in  internalExpect readMaybe msg p

natural :: Parser Char Integer
natural = internalExpect readMaybe "fatal: natural parse failed" $ some digit

anyOf :: Eq s => [s] -> Parser s s
anyOf options = satisfy (flip elem options)

-- nonZero :: Parser s [a] -> Parser s [a]
-- nonZero p = liftA2 (:) p 

exactly :: Integer -> Parser s s -> Parser s [s]
exactly = replicateM . fromIntegral

atLeast :: Integer -> Parser s s -> Parser s [s]
atLeast n p = (++) <$> exactly n p <*> many p

atMost :: Integer -> Parser s s -> Parser s [s]
atMost !n p | n <= 0 = pure []
            | otherwise = ((:) <$> p <*> atMost (n-1) p) <|> pure []

-- Note: laziness does not work in this case; ask about this.
-- atMost !n p | n <= 0 = return []
--              | otherwise = liftA2 f (optional p) (atMost (n-1) p)
--                  where f Nothing  _  = []
--                        f (Just c) cs = c:cs

atLeastMost :: Integer -> Integer -> Parser s s -> Parser s [s]
atLeastMost l u p = (++) <$> exactly l p <*> atMost (u - l) p

string :: Eq s => [s] -> Parser s [s]
string pre = P $ \s -> do
    ss <- stripPrefix pre s
    return (pre,ss)

strings :: Eq s => [[s]] -> Parser s [s]
strings = asum . fmap string

chainlMany :: Parser s a -> Parser s (a -> a -> a) -> a -> Parser s a
chainlMany pv pf v = chainlSome pv pf <|> pure v

chainrMany :: Parser s a -> Parser s (a -> a -> a) -> a -> Parser s a
chainrMany pv pf v = chainrSome pv pf <|> pure v

chainlSome :: Parser s a -> Parser s (a -> a -> a) -> Parser s a
chainlSome pv pf = pv >>= many'
    where
        many' v = some' v <|> pure v
        -- note left-associative application of pf (see chainrSome)
        some' v = (pf <*> pure v <*> pv) >>= many'

chainrSome :: Parser s a -> Parser s (a -> a -> a) -> Parser s a
chainrSome pv pf = pv >>= many'
    where
        many' v = some' v <|> pure v
        -- note right-associative application of pf (see chainlSome)
        some' v = pf <*> pure v <*> (pv >>= many')

line :: Parser Char String
line = many $ reject eol >> feed

trimLine :: Parser Char String
trimLine = do
    x <- many space
    l <- line
    optional eol
    return $ dropWhileEnd isSpace l

wrapMatch :: Eq s => s -> s -> Parser s a -> Parser s a
wrapMatch l r p = wrap (match l) (match r) p

wrap :: Parser s a -> Parser s a -> Parser s b -> Parser s b
wrap l r p = l *> p <* r

wrapNested :: Parser s a -> Parser s a -> Parser s b -> Parser s b
-- note bias towards recursive case in "res <-" clause
wrapNested l r p = wrap l r (wrapNested l r p <|> p)

instance Functor (Parser s) where
    fmap k (P f) = P $ \s -> first k <$> f s

instance Applicative (Parser s) where
    pure x = P $ \s -> Just (x, s)
    (<*>) (P ff) (P fx) = P $ \s0 -> do
        (f, s1) <- ff s0
        (x, s2) <- fx s1
        return (f x, s2)

instance Alternative (Parser s) where
    empty = P (const Nothing)
    (P fl) <|> (P fr) = P $ liftA2 (<|>) fl fr

instance Monad (Parser s) where
    return = pure
    (P f) >>= k = P $ \s0 -> do
        (x, s1) <- f s0
        unP (k x) s1
    fail _ = empty

instance MonadPlus (Parser s) where
    mzero = empty
    mplus = (<|>)